<!DOCTYPE html>
<html>
<head>
	<title>Horoscope</title>
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/superhero/bootstrap.css">
</head>
<body class="bg-secondary">
	<h1 class="text-center my-5">Welcome to Zodiac Reader</h1>
	<div class="col-lg-4 offset-lg-4">
		<form class="bg-primary p-4" action="controllers/process.php" method="POST">
			<div class="form-group">
				<label for="firstName">First Name</label>
				<input type="text" name="firstName" class="form-control">
			</div>
			<div class="form-group">
				<label for="birthMonth">Birth Month</label>
				<input type="number" name="birthMonth" class="form-control" min="1" max="12">
			</div>
			<div class="form-group">
				<label for="birthDay">Birth Day</label>
				<input type="number" name="birthDay" class="form-control" min="1" max="31">
			</div>
			<div class="text-center">
				<button type="submit" class="btn btn-success">Submit</button>
			</div>
			<?php 
				session_start();
				session_destroy();

				if(isset($_SESSION['errorMsg'])){
			?>
			<p><?php echo $_SESSION['errorMsg']?></p>

			<?php 
				}
			 ?>
		</form>

	</div>
</body>
</html>